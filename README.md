# rqt_top

#### 介绍

该插件向 ROS 主节点查询所有节点的列表，并尝试显示有关它们的进程信息。目前，信息仅适用于与 rqt 实例在同一台计算机上运行的本地节点。

可以使用顶部的文本框过滤节点列表。除非选中正则表达式框，否则节点列表将包含名称包含过滤器框中文本的任何节点。如果选中regex框，过滤器将被视为 python 样式的正则表达式，并且列表将包含名称与表达式匹配的任何节点。

将鼠标悬停在节点名称上将显示用于启动节点的完整命令。将鼠标悬停在内存上将显示节点的常驻内存和虚拟内存（以兆字节为单位）。

#### 软件架构
软件架构说明

rqt_top包含以下方法：

用于监控 ROS 进程的 RQT 插件。

![输入图片说明](https://images.gitee.com/uploads/images/2021/1030/175003_2c1f9310_1226697.png "屏幕截图_1862.png")

文件内容:
```
rqt_top
├── CHANGELOG.rst
├── CMakeLists.txt
├── mainpage.dox
├── package.xml
├── plugin.xml
├── scripts
│   └── rqt_top
├── setup.py
└── src
    └── rqt_top
```

#### 安装教程

1. 下载rpm包

aarch64:

wget https://117.78.1.88/build/home:davidhan:branches:openEuler:22.03:LTS/standard_aarch64/aarch64/ros-noetic-ros-rqt_top/ros-noetic-ros-rqt_top-0.4.10-1.oe2203.aarch64.rpm

x86_64:

wget https://117.78.1.88/build/home:davidhan:branches:openEuler:22.03:LTS/standard_x86_64/x86_64/ros-noetic-ros-rqt_top/ros-noetic-ros-rqt_top-0.4.10-1.oe2203.x86_64.rpm  

2. 安装rpm包

aarch64:

sudo rpm -ivh ros-noetic-ros-rqt_top-0.4.10-1.oe2203.aarch64.rpm --nodeps --force

x86_64:

sudo rpm -ivh ros-noetic-ros-rqt_top-0.4.10-1.oe2203.x86_64.rpm --nodeps --force

#### 使用说明

依赖环境安装:

sh /opt/ros/noetic/install_dependence.sh

安装完成以后，在/opt/ros/noetic/目录下有如下输出,则表示安装成功

输出:
```
rqt_top
├── cmake.lock
├── env.sh
├── lib
│   ├── pkgconfig
│   │   └── rqt_top.pc
│   ├── python2.7
│   │   └── dist-packages
│   └── rqt_top
│       ├── cmake.lock
│       └── rqt_top
├── local_setup.bash
├── local_setup.sh
├── local_setup.zsh
├── setup.bash
├── setup.sh
├── _setup_util.py
├── setup.zsh
└── share
    └── rqt_top
        └── cmake
```

#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request


#### 码云特技

1.  使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2.  码云官方博客 [blog.gitee.com](https://blog.gitee.com)
3.  你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解码云上的优秀开源项目
4.  [GVP](https://gitee.com/gvp) 全称是码云最有价值开源项目，是码云综合评定出的优秀开源项目
5.  码云官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6.  码云封面人物是一档用来展示码云会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
