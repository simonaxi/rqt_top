# rqt_top

#### Description


This plugin queries the ROS master for a list of all nodes, and attempts to display process information about them. Currently, information is only available for local nodes running on the same computer as the rqt instance.

The node list can be filtered using the text box at the top. Unless the regex box is checked, the node list will contain any node whose name contains the text in the filter box. If the regex box is checked, the filter will be treated as a python-style regular expression, and the list will contain any node whose name matches the expression.

Hovering over the a node name will display the full command used to start the node. Hovering over the memory will display the resident and virtual memory for the node in mebibytes.

#### Software Architecture
Software architecture description

The rqt_top contains the following methods:

RQT plugin for monitoring ROS processes.

![输入图片说明](https://images.gitee.com/uploads/images/2021/1030/175003_2c1f9310_1226697.png "屏幕截图_1862.png")

input:
```
rqt_top
├── CHANGELOG.rst
├── CMakeLists.txt
├── mainpage.dox
├── package.xml
├── plugin.xml
├── scripts
│   └── rqt_top
├── setup.py
└── src
    └── rqt_top
```

#### Installation

1.  Download RPM

aarch64:

wget https://117.78.1.88/build/home:davidhan:branches:openEuler:22.03:LTS/standard_aarch64/aarch64/ros-noetic-ros-rqt_top/ros-noetic-ros-rqt_top-0.4.10-1.oe2203.aarch64.rpm

x86_64:

wget https://117.78.1.88/build/home:davidhan:branches:openEuler:22.03:LTS/standard_x86_64/x86_64/ros-noetic-ros-rqt_top/ros-noetic-ros-rqt_top-0.4.10-1.oe2203.x86_64.rpm 

2.  Install RPM

aarch64:

sudo rpm -ivh ros-noetic-ros-rqt_top-0.4.10-1.oe2203.aarch64.rpm --nodeps --force

x86_64:

sudo rpm -ivh ros-noetic-ros-rqt_top-0.4.10-1.oe2203.x86_64.rpm --nodeps --force

#### Instructions

Dependence installation

sh /opt/ros/noetic/install_dependence.sh

Exit the following output file under the /opt/ros/noetic/ directory , Prove that the software installation is successful

```
rqt_top
├── cmake.lock
├── env.sh
├── lib
│   ├── pkgconfig
│   │   └── rqt_top.pc
│   ├── python2.7
│   │   └── dist-packages
│   └── rqt_top
│       ├── cmake.lock
│       └── rqt_top
├── local_setup.bash
├── local_setup.sh
├── local_setup.zsh
├── setup.bash
├── setup.sh
├── _setup_util.py
├── setup.zsh
└── share
    └── rqt_top
        └── cmake

```

#### Contribution

1.  Fork the repository
2.  Create Feat_xxx branch
3.  Commit your code
4.  Create Pull Request


#### Gitee Feature

1.  You can use Readme\_XXX.md to support different languages, such as Readme\_en.md, Readme\_zh.md
2.  Gitee blog [blog.gitee.com](https://blog.gitee.com)
3.  Explore open source project [https://gitee.com/explore](https://gitee.com/explore)
4.  The most valuable open source project [GVP](https://gitee.com/gvp)
5.  The manual of Gitee [https://gitee.com/help](https://gitee.com/help)
6.  The most popular members  [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
